package com.itesm.example2_2021;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class ChildActivity extends AppCompatActivity {

    String param;
    TextView myText;
    Intent receiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child);

        myText=findViewById(R.id.textView2);
        receiver=getIntent();
        myText.setText(receiver.getStringExtra("My extra"));


    }
}