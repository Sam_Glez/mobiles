package com.itesm.example2_2021;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button buttonManager;
    TextView textManager;
    ImageView imageManager;
    Intent myIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonManager=findViewById(R.id.button);
        textManager=findViewById(R.id.textView);
        imageManager=findViewById(R.id.imageView);


        buttonManager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                textManager.setText("Clicked");

                if(imageManager.getVisibility() == View.VISIBLE){
                    imageManager.setVisibility(View.INVISIBLE) ;
                }else {
                    imageManager.setVisibility(View.VISIBLE) ;
                }

                myIntent= new Intent(MainActivity.this, ChildActivity.class);
                myIntent.putExtra("My extra", textManager.getText().toString());
                startActivity(myIntent);

            }
        });
    }


}