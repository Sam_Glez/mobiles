//
//  CountryModel.swift
//  LiveListProject
//
//  Created by Samuel Octavio González Azpeitia.
//

import Foundation

struct CountryModel: Identifiable, Hashable {
    
    var id: UUID
    var name: String
    var population: String
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
