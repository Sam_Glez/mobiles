//
//  LiveListProjectApp.swift
//  LiveListProject
//
//  Created by Samuel Octavio González Azpeitia.
//

import SwiftUI

@main
struct LiveListProjectApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
